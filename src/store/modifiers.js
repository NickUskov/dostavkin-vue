const moduleModifiers = {
	state: {
		modifiers: [
			{
				id: 1,
				name: 'Burger modifiers',
				price: '',
				min: 1,
				default: 0,
				max: 99,
				required: true,
				visible: true,
				children: [
					{
						id: 11,
						name: 'bacon',
						price: 60,
						min: 1,
						default: 0,
						max: 99,
						required: true,
						visible: true,
					},
					{
						id: 12,
						name: 'chease',
						price: 60,
						min: 1,
						default: 0,
						max: 99,
						required: true,
						visible: true,
					}
				]
			},
			{
				id: 2,
				name: 'Размер пицц и пирогов',
				price: '',
				min: 1,
				default: 0,
				max: 1,
				required: false,
				visible: true,
				children: [
					{
						id: 22,
						name: '24 см',
						price: 200,
						min: 0,
						default: 0,
						max: 1,
						required: true,
						visible: true,
					},
					{
						id: 23,
						name: '30 см',
						price: 300,
						min: 0,
						default: 0,
						max: 1,
						required: false,
						visible: true,
					},
				]
			}

		]
	},
	mutations: {
		setModifiers(state, value) {
			state.modifiers = value.map(el => el)
		}
	},
	getters: {}
}

export default moduleModifiers