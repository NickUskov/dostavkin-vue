import { createStore } from 'vuex'
import moduleProducts from "./products";
import moduleModifiers from "./modifiers";

export default createStore({
  state: {

  },
  mutations: {

  },
  actions: {
  },
  modules: {
    products: moduleProducts,
    modifiers: moduleModifiers
  }
})
