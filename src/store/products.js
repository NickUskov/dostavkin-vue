const moduleProducts = {
	state: {
		categories: [
			{id: 1, name: 'All', children: [], count: 13, active: false},
			{id: 1, name: 'Десерты', children: [], count: 13, active: false},
			{id: 1, name: 'Завтраки', children: [], count: 13, active: false},
			{id: 1, name: 'Основные блюда', children: [], count: 13, active: false},
			{
				id: 1, name: 'Напитки', children: [
					{id: 1, name: 'Горячие', children: [], count: 13, active: false},
					{id: 1, name: 'Холодные', children: [], count: 13, active: false},
				],
				count: 5, active: false
			},
		]
	},
	mutations: {
		setActive(state, payload) {
			state.categories.forEach(cat => {
				if(cat === payload) {
					cat.active = true
					} else {
					cat.active = false
				}
			})
		}
	},
	getters: {}
}

export default moduleProducts