let messages = {
    en: {
        errors: {
            required: 'This field is required',
            email: 'Incorrect email address',
            length: 'This field must contain at least',
            pass:  'The passwords must match'
        },
        message: {
            login: 'Sign In',
            email: 'Email',
            password: 'Password',
            authorization: 'login',
            noAccount: 'Don`t have an account?',
            forgotPassword: 'Forgot password?'
        }
    },
    ru: {
        errors: {
            required: 'Это поле обязательно',
            email: 'Некорректный адрес электронный почты',
            length: 'Поле должно содерать как минимум',
            pass:  'Пароли должны совпадать'
        },
        message: {
            login: 'Войти',
            email: 'Электронная почта',
            password: 'Пароль',
            authorization: 'Авторизация',
            noAccount: 'Нет аккаунта?',
            forgotPassword: 'Забыли пароль?'

        }
    }
}

export default messages