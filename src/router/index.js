import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/main',
    name: 'Main',
    meta: {layout: 'panel-layout'},
    component: () =>import('../views/panel/Dashboard')
  },
  {
    path: '/categories',
    name: 'Categories',
    meta: {layout: 'panel-layout'},
    component: () =>import('../views/panel/Categories')
  },
  {
    path: '/modifiers',
    name: 'Modifiers',
    meta: {layout: 'panel-layout'},
    component: () =>import('../views/panel/Modifiers')
  },
  {
    path: '/products',
    name: 'Producs',
    meta: {layout: 'panel-layout'},
    component: () =>import('../views/panel/Products')
  },
  {
    path: '/products/new',
    name: 'NewProduct',
    meta: {layout: 'panel-layout'},
    component: () =>import('../views/panel/NewProduct')
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/login',
    name: 'Login',
    meta: {layout: 'auth-layout'},
    component: () =>import('../views/auth/Login')
  },
  {
    path: '/reset',
    name: 'Reset',
    meta: {layout: 'auth-layout'},
    component: () =>import('../views/auth/PasswordReset')
  },
  {
    path: '/register',
    name: 'Register',
    meta: {layout: 'auth-layout'},
    component: () =>import('../views/auth/Register')
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
