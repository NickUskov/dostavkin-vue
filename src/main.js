import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './assets/css/tailwind.css'
import Maska from 'maska'

import AppAccordion from "./components/panel/AppAccordion";
import AppAccordionItem from "./components/panel/AppAccordionItem";

//I18n translator
import { createI18n } from 'vue-i18n'
import messages from "./locales";

// 2. Create i18n instance with options
const i18n = createI18n({
    legacy: false,
    locale: 'ru', // set locale
    fallbackLocale: 'en', // set fallback locale
    messages, // set locale messages
    // If you need to specify other options, you can set other options
    // ...
})

const app = createApp(App)

app.component('app-accordion', AppAccordion)
app.component('app-accordion-item', AppAccordionItem)

app.use(store)
app.use(router)
app.use(Maska)
app.use(i18n)
app.mount('#app')